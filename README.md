# EVOJAM Directory Browser

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install` then `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## stack:
## RTKQuery - i choose RTK Query because it makes easier to fetching data and it also has a lot of nice features like caching tool.
## StyledComponents - My Favorite styling tool, maybe not as fast like Tailwind but in my opinion with styled Components code looks much nicer.
## Typescript and React - I chose React because I am currently learning React and i also choose Typescript instead of javascript to get rid of problems with, for example types :).

