import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { directoryApi } from "./directories";
export const store = configureStore({
  reducer: {
    [directoryApi.reducerPath]: directoryApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(directoryApi.middleware),
});

setupListeners(store.dispatch);
