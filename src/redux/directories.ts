import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const directoryApi = createApi({
  reducerPath: "directory",
  baseQuery: fetchBaseQuery({
    baseUrl: `https://fnp5vd20r2.execute-api.us-east-1.amazonaws.com/dev/`,
  }),
  endpoints: (builder) => ({
    getDirectoryById: builder.query<any, string>({
      query: (id) => `directories/${id}`,
    }),
  }),
});

export const { useGetDirectoryByIdQuery } = directoryApi;
