import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { GlobalStyles } from "./assets/styles/GlobalStyle";
import { store } from "./redux/store";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import DirectoryPath from "./Components/DirectoryPath/DirectoryPath";
const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <GlobalStyles />
      <Router>
        <DirectoryPath />
        <App />
      </Router>
    </Provider>
  </React.StrictMode>
);
