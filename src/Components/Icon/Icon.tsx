import * as S from "./IconStyles";
type Props = {
  name: string;
};
const Icon = ({ name }: Props) => {
  const regex = new RegExp(/[^.]+$/);
  const returnValidIcon = (fileName: string) => {
    const extension = fileName.match(regex);
    let fileExtension = "";
    if (extension) fileExtension = extension[0];
    switch (fileExtension) {
      case "jpg": {
        return "/photo.svg";
      }
      case "directory": {
        return "/folder.svg";
      }
      case "txt": {
        return "/file.svg";
      }
      default: {
        return "/file.svg";
      }
    }
  };
  return (
    <S.StyledIcon>
      <S.StyledImg src={returnValidIcon(name)} alt="File icon" />
    </S.StyledIcon>
  );
};

export default Icon;
