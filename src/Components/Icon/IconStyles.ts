import styled from "styled-components";

export const StyledIcon = styled.div`
  width: 40px;
  height: 40px;
  place-self: center;
`;
export const StyledImg = styled.img`
  width: 100%;
  height: 100%;
`;
