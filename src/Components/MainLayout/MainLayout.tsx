import * as S from "./MainlayoutStyles";
import { Outlet } from "react-router-dom";

const MainLayout = () => {
  return (
    <S.StyledLayout>
      <Outlet />
    </S.StyledLayout>
  );
};

export default MainLayout;
