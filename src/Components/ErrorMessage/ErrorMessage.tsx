import {
  StyledErrorWrapper,
  StyledErrorLine,
} from "../../assets/styles/ReusableStyles";
const ErrorMessage: Component<any> = ({ error }) => {
  if (error) console.error(error);
  return (
    <>
      {error && (
        <StyledErrorWrapper>
          <StyledErrorLine>
            Unable to fetch directory with given id.
          </StyledErrorLine>
          <StyledErrorLine>Error Code: {error.originalStatus}</StyledErrorLine>
          <StyledErrorLine>{error.error}</StyledErrorLine>
        </StyledErrorWrapper>
      )}
    </>
  );
};

export default ErrorMessage;
