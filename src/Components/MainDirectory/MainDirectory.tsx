import { useLocation, useNavigate, useParams } from "react-router-dom";
import type {
  SerializedError,
  FetchBaseQueryError,
  FetchData,
} from "../../@types/reduxTypes";
import * as S from "../../assets/styles/ReusableStyles";
import Icon from "../Icon/Icon";
import { useGetDirectoryByIdQuery } from "../../redux/directories";
import ErrorMessage from "../ErrorMessage/ErrorMessage";
type FetchedData = {
  data?: FetchData | undefined;
  error?: FetchBaseQueryError | SerializedError | undefined;
};

type Location = {
  hash: string;
  key: string;
  pathname: string;
  search: string;
  state: any;
};
const MainDirectory = () => {
  let location: Location = useLocation();
  let navigate = useNavigate();
  let { id } = useParams();
  const endpointId = id ? id : "";
  const { data, error }: FetchedData = useGetDirectoryByIdQuery(endpointId);
  const redirect = (redirectId: string, name: string) => {
    const directory = location.state
      ? `${location.state.directory}/${name}`
      : `root/${name}`;
    const navigateId = location.state
      ? `${location.state.id}/${redirectId}`
      : `/${redirectId}`;
    navigate(`/${redirectId}`, {
      state: {
        directory,
        id: navigateId,
      },
    });
  };
  return (
    <S.StyledDirectory>
      {error ? (
        <ErrorMessage error={error} />
      ) : (
        <>
          {data &&
            data.files.map(({ name }: { name: string }, index: string) => (
              <S.StyledFile key={name + index}>
                <Icon name={name} />
                <S.StyledName>{name}</S.StyledName>
              </S.StyledFile>
            ))}
          {data &&
            data.directories.map(
              ({ id, name }: { id: string; name: string }) => (
                <S.StyledFile onClick={() => redirect(id, name)} key={id}>
                  <Icon name="directory" />
                  <S.StyledName>{name}</S.StyledName>
                </S.StyledFile>
              )
            )}
        </>
      )}
    </S.StyledDirectory>
  );
};

export default MainDirectory;
