import styled from "styled-components";
export const StyledPathContainer = styled.div`
  padding: 1rem 0rem 5rem 2rem;
  display: flex;
  align-items: center;
`;
export const StyledPath = styled.p`
  font-size: 2.5rem;
  cursor: pointer;
`;
export const StyledButton = styled.span`
  width: max-content;
  height: 100%;
  display: grid;
  place-content: center;
  margin-left: 1rem;
`;
export const StyledImage = styled.img`
  fill: white;
`;
