import { useLocation, useNavigate } from "react-router-dom";
import * as S from "./DirectoryPathStyles";
const DirectoryPath = () => {
  let location: any = useLocation();
  let navigate = useNavigate();
  const path = location.state ? location.state.directory.split("/") : "root";
  const id = location.state ? location.state.id.split("/") : "";
  const redirect = (redirectId: string, index: number) => {
    path.length = index + 1;
    id.length = index + 1;
    navigate(`/${redirectId}`, {
      state: {
        directory: path.join("/"),
        id: id.join("/"),
      },
    });
  };
  const returnRightId = (id: string | string[], index: number) => {
    if (Array.isArray(path)) {
      return id[index];
    } else {
      return path;
    }
  };
  return (
    <S.StyledPathContainer>
      {Array.isArray(path) ? (
        path.map((pathName, index) => (
          <S.StyledPath
            onClick={() => redirect(returnRightId(id, +index), +index)}
            key={pathName + index}
          >
            {pathName} /
          </S.StyledPath>
        ))
      ) : (
        <S.StyledPath>{path}</S.StyledPath>
      )}
    </S.StyledPathContainer>
  );
};

export default DirectoryPath;
