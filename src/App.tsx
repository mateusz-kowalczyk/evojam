import MainDirectory from "./Components/MainDirectory/MainDirectory";
import type { RouteObject } from "react-router-dom";
import { useRoutes } from "react-router-dom";
import MainLayout from "./Components/MainLayout/MainLayout";
function App() {
  const routes: RouteObject[] = [
    {
      path: "/",
      element: <MainLayout />,
      children: [
        { path: "/", element: <MainDirectory /> },
        { path: "/:id", element: <MainDirectory /> },
      ],
    },
  ];
  return useRoutes(routes);
}

export default App;
