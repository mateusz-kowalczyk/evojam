export type SerializedError = {
  name?: string;
  message?: string;
  stack?: string;
  code?: string;
};
export type FetchBaseQueryError =
  | {
      status: number;
      data: unknown;
    }
  | {
      status: "FETCH_ERROR";
      data?: undefined;
      error: string;
    }
  | {
      status: "PARSING_ERROR";
      originalStatus: number;
      data: string;
      error: string;
    }
  | {
      status: "CUSTOM_ERROR";
      data?: unknown;
      error: string;
    };

export type FetchData = {
  name: string;
  files?: any;
  id: string;
  directories: DirectoriesType[];
};
type DirectoriesType = {
  name: string;
  id: string;
};
