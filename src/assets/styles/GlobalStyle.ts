import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
body{
background: #282c34;
color:white;
}
*, ::before, ::after{
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    outline: 0;
    border: 0;
}
li{
    list-style: none;
}
a{
    text-decoration: none;
}
`;
