import styled from "styled-components";
import { Link } from "react-router-dom";
export const StyledFile = styled.div`
  display: grid;
  place-content: center;
  max-width: 70px;
  gap: 10px;
`;
export const StyledName = styled.div`
  white-space: nowrap;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  text-align: center;
  color: white;
`;

export const StyledDirectory = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, 70px);
  place-content: center;
  gap: 30px;
`;
export const StyledLink = styled(Link)`
  color: white;
  text-decoration: none;
`;
export const StyledErrorWrapper = styled.div`
  width: 100%;
  display: grid;
  place-content: center;
`;
export const StyledErrorLine = styled.div`
  width: max-content;
`;
